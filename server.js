var PORT = process.env.PORT || 3000;
var moment = require('moment');
var express = require('express');
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);

app.use(express.static(__dirname + '/public'));

var clientInfo = {};

// Sends current users to provided socket
function sendCurrentUsers (socket) {
  var info = clientInfo[socket.id];
  var users = [];

  if (typeof info === 'undefined') {
    return;
  }

  Object.keys(clientInfo).forEach(function(socketId) {
    var userInfo = clientInfo[socketId];

    if (info.room === userInfo.room) {
      users.push(userInfo.name);
    }
  });

  socket.emit('message', {
    name: 'Sistema',
    text: 'Usuários ON: ' + users.join(', '),
    timestamp: moment.valueOf()
  });
}

io.on('connection', function(socket) {
  console.log('Usuário conectado via socket.io!');

  socket.on('disconnect', function() {
    var userData = clientInfo[socket.id];

    if (typeof userData !== 'undefined') {
      socket.leave(userData.room);
      io.to(userData.room).emit('message', {
        name: 'Sistema',
        text: userData.name + ' Saiu!',
        timestamp: moment.valueOf()
      });
      delete clientInfo[socket.id];
    }
  });

  socket.on('joinRoom', function(req) {
    clientInfo[socket.id] = req;
    socket.join(req.room);
    socket.broadcast.to(req.room).emit('message', {
      name: 'Sistema',
      text: req.name + ' entrou',
      timestamp: moment().valueOf()
    });
  });

  socket.on('message', function(message) {
    console.log('Mensagem Recebida: ' + message.text);

    if (message.text === '@usuarios') {
      sendCurrentUsers(socket);
    } else {
      message.timestamp = moment().valueOf();
      io.to(clientInfo[socket.id].room).emit('message', message);
    }
  });

  // timestamp property - JavaScript timestamp (mileseconds)

  socket.emit('message', {
    name: 'Sistema',
    text: 'Bem-Vindo ao Canalhas Chat!',
    timestamp: moment().valueOf()
  });
});

http.listen(PORT, function() {
  console.log('Servidor Iniciado...');
});
